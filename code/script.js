//Секундомер

const mytimer = document.querySelector(".display");
const hours = mytimer.querySelector(":first-child"); 
const minutes = mytimer.querySelector(":nth-child(2)"); 
const seconds = mytimer.querySelector(":last-child"); 


let hour = 00, minute = 00, second = 00, intervalHandler;

let createTimer = () => {
	second++;
	seconds.innerText = '0' + second;

	if (second > 9) {
		seconds.innerText = second;
	}
	if (second > 59) {
		second = 0;
		minute++;
		minutes.innerText = '0' + minute;		
	}
	if (minute > 9) {
		minutes.innerText = minute;
	}
	
	if (minute > 59) {
		minute = 0;
		hour++;
		hours.innerText = '0' + hour;		
	}
	if (hour > 9) {
		hours.innerText = hour;		
	}
	if (hour > 23) {
		hour = 0;		
	}
};

const btnStart = document.getElementById("start");
const btnStop = document.getElementById("stop");
const btnReset = document.getElementById("reset");

btnStart.onclick = () => {	
	intervalHandler = setInterval(createTimer, 1000);
	mytimer.classList.remove("displayStop", "displayReset");
  };
  
  btnStop.onclick = () => {
	clearInterval(intervalHandler);
	mytimer.classList.add("displayStop");
	mytimer.classList.remove("displayReset");
  };
  
  btnReset.onclick = () => {
	clearInterval(intervalHandler);
	hour = 00;
	minute = 00;
	second = 00;
	hours.innerText = "00";
	minutes.innerText = "00";
	seconds.innerText = "00";
	mytimer.classList.add("displayReset");
	mytimer.classList.remove("displayStop");
  };

 //Проверка номера телефона 

  	let button1 = document.querySelector("input");
	let button2 = document.createElement("input");
	let errorEnter = document.createElement("div");
	button2.type = "button";
	button2.value = "SAVE";
	errorEnter.style.fontSize = "2.2rem";
	errorEnter.style.color = "red";
	
	button1.after(button2);
	
	let pattern = /^\d{3}-\d{3}-\d\d-\d\d$/;
	
	button2.onclick = () => {
		
		if (pattern.test(button1.value)) {
			button1.style.backgroundColor = "green";
			document.location = "https://www.youtube.com/";			
		}else {
			errorEnter.innerText = "ERROR";
			button1.before(errorEnter);
		}
	}

//Слайдер

window.onload = () => {
        let img = document.createElement("img");
		img.setAttribute("src", 'https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg',);
		img.style.width = "30rem";
				        
        let imgArr = ["https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg",
		"https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
		"https://naukatv.ru/upload/files/shutterstock_418733752.jpg",
		"https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg",
		"https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg",];
		
        let i = 0;
        setInterval(()=>{
        i++;
        if (i === imgArr.length) i = 0;
        img.setAttribute("src", imgArr[i]);             
        }, 1000);
		document.body.append(img);
}

